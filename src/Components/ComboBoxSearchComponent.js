import ko from 'knockout';
import ComboBoxSearchViewModel from '../ViewModels/ComboBoxSearchViewModel';
import ComboBoxSearchTemplate from '../Templates/ComboBoxSearchTemplate.html';
import '../Styles/ComboBoxSearch.css';

export default ko.components.register("combobox-search", {
    viewModel: {
        createViewModel: function(params, componentInfo) {
            // - 'params' is an object whose key/value pairs are the parameters
            //   passed from the component binding or custom element
            // - 'componentInfo.element' is the element the component is being
            //   injected into. When createViewModel is called, the template has
            //   already been injected into this element, but isn't yet bound.
            // - 'componentInfo.templateNodes' is an array containing any DOM
            //   nodes that have been supplied to the component. See below.
            params.element = componentInfo.element;
            return new ComboBoxSearchViewModel(params);
        }
    },
    template: ComboBoxSearchTemplate
});
    