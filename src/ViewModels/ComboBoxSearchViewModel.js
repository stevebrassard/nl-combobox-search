import ko from 'knockout';
import postbox from 'knockout-postbox';
import SearchViewModel from '../ViewModels/SearchViewModel';
import {debounce} from '../Common/Utilities';

export default class ComboBoxSearchViewModel extends SearchViewModel {
    constructor(props) {
        super(props);
        this.props = props;
        this.pageSize = props.pageSize || 100;
        this.placeholder = props.placeholder || null;
        this.readOnly = props.readOnly || false;
        this.disabled = props.disabled || false; //used to make control behave more like a dropdown instead of combobox
        this.selectedItem = ko.observable(null).publishOn(`${this.props.element.id}-OnSelected`); //When this value changes (in parent)....(see below)
        this.subscriberItem = ko.observable();
        if(this.props.subscribeToId) {
            this.subscriberItem.subscribeTo(`${this.props.subscribeToId}-OnSelected`);  //...(con't) This item gets updated (in child)
        }
        this.scrolled = debounce(this.scrolled, 120);
        this.selectItem = this.selectItem.bind(this); //bind this instance to the selectItem method
        this.searchText = ko.observable(); //.extend({ rateLimit: 100});
        this.isComponentLoaded = ko.observable(false);
        this.cancelScrollRequest = false;
        this.cachedResult = {};
        this.attachEventHandlers();
        this.attachKnockoutBindings();

        this.searchText.subscribe((function(query = "") {    
            if(this.selectedItem() && query !== this.selectedItem().name) { this.selectedItem(null); };  
            this.cancelScrollRequest = false;
            this.executeSearch(query);
        }).bind(this));

        this.subscriberItem.subscribe((function(data) {
            this.searchText("");
            this.executeSearch();
        }).bind(this));

        //execute default fetch to pull all data from service 
        let request = { url: this.urlBuilder(this.props.serviceEndpoint, "", 1, this.pageSize, null, 'asc')};
        this.fetch(request).done(function(response) {
                let resultData = this.getMappedData(response);
                this.results(resultData);   
                this.cachedResult[request.url] = resultData;
                this.isComponentLoaded(true);
        });
    }

    //Override Base Implementation
    executeSearch(query) {
        if(this.subscriberItem()) {
            let request = { url: this.urlBuilder(`${this.props.serviceEndpoint}?orgid=${this.subscriberItem().id}`, query, 1, this.pageSize, null, 'asc')};
            if(this.hasCachedData(request.url)) {return;} //handle cache
            this.fetch(request).done(function(response) {
                let resultData = this.getMappedData(response);
                this.results(resultData);  
                this.cachedResult[request.url] = resultData;
            });                
        } else {
            let request = {url: this.urlBuilder(this.props.serviceEndpoint, query, 1, this.pageSize, null, 'asc')};
            if(this.hasCachedData(request.url)) {return;} //handle cache
            this.fetch(request).done(function(response) {
                let resultData = this.getMappedData(response);
                this.results(resultData); 
                this.cachedResult[request.url] = resultData;
            });            
        } 
    }

    //Override Base Implementation
    getMappedData(response) {
        return response.map( (data, key) =>  { 
            data.isActive = ko.observable(false);
            return data;
        });
    }

    scrolled(data, event) {       
        let elem = $(event.target);
        if (elem.scrollTop() !== 0 && elem.scrollTop() > (elem[0].scrollHeight - elem[0].offsetHeight - 40)) { 
            this.page++;          
            var ajaxOptions = {
                url: this.urlBuilder(this.props.serviceEndpoint, this.searchText(), this.page, this.pageSize, null, 'asc'),
            };    
            if(!this.cancelScrollRequest) {
                if(this.hasCachedData(ajaxOptions.url, true)) return; //handle cache
         
                this.fetch(ajaxOptions).done( (response) => {
                    if(response.length == 0) {
                        this.cancelScrollRequest = true;
                    } else {
                        let resultData = this.getMappedData(response);
                        this.cachedResult[ajaxOptions.url] = resultData;
                        let appendedResults = this.results().concat(resultData);
                        this.results(appendedResults);                        
                    }
                });
            }
        }   
    }     

    hasCachedData(key, shouldAppend = false) {
        if(this.cachedResult) {
            let response = this.cachedResult[key];
            if(response) {
                let resultData = this.getMappedData(response);
                shouldAppend ? this.results(this.results().concat(resultData)) :  this.results(resultData);            
                return true;                                      
            }
        }   
        return false;    
    }

    clearSelection(data, event) {
        this.page = 1;
        this.cancelScrollRequest = false;
        this.selectedItem().isActive(false);
        this.searchText(""); 
        this.selectedItem(null);
        $(`#${this.props.element.id} ul.dropdown-menu > li.active`).removeClass('active');
        $('#'+this.props.element.id).find('input.combobox').val(""); 
        $(`#${this.props.element.id} ul.dropdown-menu`)[0].scrollTop = -1;
        event.stopPropagation();
        event.preventDefault();            
    }

    selectItem(data, event) {
        this.searchText.silentUpdate(data.name);
        $('#'+this.props.element.id).find('input.combobox').val(data.name);
        this.selectedItem(data);
        data.isActive(true);
        this.results([data]);
        $(`#${this.props.element.id} ul.dropdown-menu > li:first-child`).addClass('active');
    }

    removePreviousSelection() {
        let selectedItem = ko.utils.arrayFirst(this.results(), (item) => {
            return item.isActive() === true;
        });
        selectedItem ? selectedItem.isActive(false) : null;             
    }

    attachEventHandlers() {
        $(this.props.element).on('hide.bs.dropdown', (function(e) {              
            //If a selection is not made, and the input has/has not text on close of dropdown-menu, reset the input to empty
            if(this.selectedItem() === null) { //} && !!this.searchText()) {
                this.page = 1;
                this.cancelScrollRequest = false;                 
                this.searchText(""); //clear input and request page 1 data
                $(`#${this.props.element.id} ul.dropdown-menu`)[0].scrollTop = -1;
            }            
        }).bind(this));    
        
        //Override the default bootstrap "toggle" class behavior when we have the menu open
        $(this.props.element).find("input.combobox").on('click focus', (function(e) {
            //find other open dropdowns, and close them
            $(".dropdown-menu:visible").each(function(index) {
                $(this).parent().removeClass("open");
            });            
            let dropdownMenu = $(e.currentTarget).parent().children(".dropdown-menu");
            if(dropdownMenu.is(":visible")) {
                dropdownMenu.parent().addClass("open");
            }
            e.cancelBubble=true;
            e.stopPropagation();
            e.preventDefault();    
            e.stopImmediatePropagation();  
        }).bind(this));

        //Enable keyboard navigation
        $(this.props.element).find('.input-group input').keyup((function(e){                           
            if(e.which == 46) { // delete          
                if(this.selectedItem() !== null && this.searchText() !== this.selectedItem().text) {
                    this.clearSelection(null, e); 
                }
            }
            let input = $(this.props.element).find("input.combobox")[0];
            if(e.which == 8 && !input.value) { //backspace (while clearing out input)
                this.clearSelection(null, e);
            }    
        }).bind(this));

        $(this.props.element).find('.input-group input').keydown(function(e){
            if(e.which == 9 || e.which == 40){ // tab || down_arrow
                e.preventDefault();
                $(this).parent().find('.dropdown-toggle').click();
                $(this).parent().find('.dropdown-menu a:first').focus();
            }            
        });

        $(this.props.element).find('.dropdown-menu a').keydown(function(e){
            switch(e.which){
                case 36: // home
                    e.preventDefault();
                    $(this).closest('.dropdown-menu').find('a:first').focus();
                break;
                case 35: // end
                    e.preventDefault();
                    $(this).closest('.dropdown-menu').find('a:last').focus();
                break;
            }
        });         
    }

    attachKnockoutBindings() {
        ko.observable.fn.silentUpdate = function(value) {
            this.notifySubscribers = function() {};
            this(value);
            this.notifySubscribers = function() {
                ko.subscribable.fn.notifySubscribers.apply(this, arguments);
            };
        };         
    }
}