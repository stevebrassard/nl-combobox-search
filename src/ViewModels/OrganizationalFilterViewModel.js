import ko from 'knockout';
import * as postbox from 'knockout-postbox';


//Manages the selected set of combobox-search filters
export default class OrganizationalFilterViewModel {
    constructor(props) {
        this.props = props;
        this.orgs = ko.observable(null).subscribeTo(`${props.organizationsid}-OnSelected`);
        this.depts = ko.observable(null).subscribeTo(`${props.departmentsid}-OnSelected`);
        this.pgs = ko.observable(null).subscribeTo(`${props.peoplegroupsid}-OnSelected`);
        this.jts = ko.observable(null).subscribeTo(`${props.jobtitlesid}-OnSelected`);

        this.selectedFilters = ko.computed(() => {
            return {organization: this.orgs(), department: this.depts(), peoplegroup: this.pgs(), jobtitles: this.jts()};
        });
        this.selectedFilters.publishOn("OrganizationalFilterUpdated");
    }
}