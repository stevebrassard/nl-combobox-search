import ko from 'knockout';
import * as postbox from 'knockout-postbox'
import {findObject} from '../Common/Utilities';

export default class SearchViewModelBase {
    constructor(props) {
        this.props = props;
        this.searchText = ko.observable().subscribeTo("SearchTextModified");
        this.results = ko.observableArray(); //this may get overrided in derived classes
        this.isSearching = ko.observable(false);
    }

    executeSearch(query) 
    {  
        console.log("Search Executed", query);
        let ajaxOptions = {
            url: this.urlBuilder(this.props.serviceEndpoint, query, 1, this.pageSize, null, 'asc')
        };
        return this.fetch(ajaxOptions); 
    }    

    fetch(ajaxOptions) {
        let ajaxDefaults = {
            dataType: "json",
            type: "GET",
            contentType: "application/json; charset=utf-8",  
            beforeSend: function() {
                this.isSearching(true);
            },
            complete: function() {
                this.isSearching(false);
            },            
            context: this          
        }
        var ajaxOptions = $.extend(true, {}, ajaxDefaults, ajaxOptions);
        //console.log("Calling fetch: ", ajaxOptions);
        var searchPromise = $.ajax(ajaxOptions);
        return searchPromise;
    }

    //This urlBuilder method is for use with json-server for testing purposes. It may not work with other API's
    urlBuilder(serviceUrl, query, page, limit, sort, order) {
        var parameters = [], url = `${serviceUrl}`, p = null;
        if(!url.includes("?")) {url = url.concat("?");}
        if(query) parameters.push(`q=${query}`);
        if(page) parameters.push(`_page=${page}`);        
        if(limit) parameters.push(`_limit=${limit}`);
        if(sort) parameters.push(`sort=${sort}`);
        if(order) parameters.push(`order=${order}`);        
        if(url.substr(url.length - 1) !== "?") {url = url.concat("&");}
        for(p in parameters) {
            url = url.concat(`${parameters[p]}&`);
        }
        return url.substring(0, url.length - 1);
    }

    getMappedData(response) {
        //Dynamically map response data to datasource mapping object properties (if mapping is required)
        try
        {            
            let mapping = this.props.datasource.remote.data.mapping; //if props doesnt have datasource.remote.mapping, exception occurs
            if(this.props.datasource.remote.data.dataWrapper) {
                response = findObject(response, this.props.datasource.remote.data.dataWrapper); //unwrap data if response contains a wrapped response. (ex. response.d.data)  (dataWrapper should be a string like "d.data")             
            }
            if(mapping) {
                response = response.map( (data, key) =>  {              
                    var mappedObject = new Object();
                    for (var key in mapping) {
                        if (mapping.hasOwnProperty(key)) {
                            mappedObject[key] = data[mapping[key]]; //for each key in datasource mapping, get the value from the mapping found in the response data
                        }
                    }
                    return mappedObject;            
                });  
            };
            return response;            
        }   
        catch(ex) {
            //No specific wrapped object exists defined in markup we need to map to
            return response; 
        }    
    }
}
