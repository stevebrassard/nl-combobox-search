import ko from 'knockout';
import PageViewModel from './ViewModels/PageViewModel';
import ComboBoxSearchComponent from './Components/ComboBoxSearchComponent';
import OrganizationalFilterComponent from './Components/OrganizationalFilterComponent';

ko.applyBindings(new PageViewModel(), document.getElementById('root')); //represents some VM the componets belong to